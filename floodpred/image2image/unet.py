# -*- coding=utf-8 -*-
from utils.train_utils import mean_wdepth_diff
import tensorflow as tf
import pickle
from tensorflow import keras
from tensorflow.keras.models import Model
from tensorflow.keras.backend import int_shape
from tensorflow.keras.layers import (
    BatchNormalization, Conv2D, Conv2DTranspose,
    MaxPooling2D, Dropout, Input, concatenate, Cropping2D,
    SpatialDropout2D
)


def conv2d_block(
    inputs,
    use_batch_norm=True,
    dropout=0.3,
    dropout_type="spatial",
    filters=16,
    kernel_size=(3, 3),
    activation="relu",
    kernel_initializer="he_normal",
    padding="same",
):

    if dropout_type == "spatial":
        DO = SpatialDropout2D
    elif dropout_type == "standard":
        DO = Dropout
    else:
        raise ValueError(
            f"dropout_type must be one of ['spatial', 'standard'], got {dropout_type}"
        )

    c = Conv2D(
        filters,
        kernel_size,
        activation=activation,
        kernel_initializer=kernel_initializer,
        padding=padding,
        use_bias=not use_batch_norm,
    )(inputs)
    if use_batch_norm:
        c = BatchNormalization()(c)
    if dropout > 0.0:
        c = DO(dropout)(c)
    c = Conv2D(
        filters,
        kernel_size,
        activation=activation,
        kernel_initializer=kernel_initializer,
        padding=padding,
        use_bias=not use_batch_norm,
    )(c)
    if use_batch_norm:
        c = BatchNormalization()(c)
    return c

def get_crop_shape(target, refer):
    # width, the 3rd dimension
    cw = target[2] - refer[2]
    assert (cw >= 0)
    if cw % 2 != 0:
        cw1, cw2 = int(cw/2), int(cw/2) + 1
    else:
        cw1, cw2 = int(cw/2), int(cw/2)
    # height, the 2nd dimension
    ch = target[1] - refer[1]
    assert (ch >= 0)
    if ch % 2 != 0:
        ch1, ch2 = int(ch/2), int(ch/2) + 1
    else:
        ch1, ch2 = int(ch/2), int(ch/2)

    return (ch1, ch2), (cw1, cw2)

def build_unet(
    input_shape,
    num_classes=1,
    dropout=0.5, 
    filters=64,
    num_layers=4,
    output_activation='sigmoid'): # 'sigmoid' or 'softmax'

    # Build U-Net model
    inputs = Input(input_shape)
    x = inputs   

    down_layers = []
    for l in range(num_layers):
        # Jay: adjust use batch norm into True
        x = conv2d_block(inputs=x, filters=filters, use_batch_norm=True, dropout=0.0, padding='same')
        down_layers.append(x)
        x = MaxPooling2D((2, 2), strides=2) (x)
        filters = filters*2 # double the number of filters with each layer

    x = Dropout(dropout)(x)
    x = conv2d_block(inputs=x, filters=filters, use_batch_norm=True, dropout=0.0, padding='same')

    for conv in reversed(down_layers):
        filters //= 2 # decreasing number of filters with each layer 
        x = Conv2DTranspose(filters, (2, 2), strides=(2, 2), padding='same') (x)
        
        ch, cw = get_crop_shape(int_shape(conv), int_shape(x))
        conv = Cropping2D(cropping=(ch, cw))(conv)

        x = concatenate([x, conv])
        x = conv2d_block(inputs=x, filters=filters, use_batch_norm=True, dropout=0.0, padding='same')
    
    # outputs = Conv2D(num_classes, (1, 1), activation=output_activation) (x)    
    outputs = Conv2D(num_classes, (1, 1), padding='same') (x)    
    
    model = Model(inputs=[inputs], outputs=[outputs])
    return model


class UnetAgent():
    def __init__(self, input_shape=None):
        self.model = build_unet(input_shape)

    def learn_from(self, dataset, LossHistory, val=None, outpath='.'):
        self.model.compile(
            optimizer=keras.optimizers.Adam(),
            # check default regularzation
            loss=keras.losses.MeanSquaredError(),
            # loss = mean_wdepth_diff,
            # TODO change loss into mean water depth for pixels 
            # on which water depth more than 1 meters 
            # keras.metrics.MeanSquaredError(), 
            metrics= [mean_wdepth_diff]
        )

        self.model.summary()
        print('[INFO] training sdato!')
        history = self.model.fit(dataset, 
                                 validation_data=val, 
                                 epochs=300,)
                                #  callbacks=[LossHistory])

        with open('history.pickle', 'wb') as f:
            pickle.dump(history.history, f)

        self.model.save(outpath)
        print('[INFO] training end , model saved at ', outpath)
        
    def load(self, path):
        self.model = tf.keras.models.load_model(
            path,
            custom_objects={
                'mean_wdepth_diff':mean_wdepth_diff
            }
        )

    def test(self, data):
        self.model.evaluate(data)

    def predict(self, data):
        '''
        return: groundtruth water depth numpy array and prediction numpy array
        '''
        pred = self.model.predict(data)
        print('predicting finished, shape of prediction is {}'\
                .format(pred.shape))

        return pred
