import os

import matplotlib.pyplot as plt
import numpy as np
# from utils.data_utils import split_data

def save_viz(figures, outputpath):
    if not os.path.exists(outputpath):
        os.makedirs(outputpath)
    print("[INFO] visuallization")
    for i, fig in enumerate(figures):
        # decrease dim
        if fig.shape[0] == 1:
            fig = fig[0]

        outname = os.path.join(outputpath, str(i) +'.jpg')
        # hm = None
        # hm = cv2.normalize(fig, hm, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
        # hm = cv2.applyColorMap(hm, cv2.COLORMAP_JET)
        # hm = cv2.cvtColor(hm, cv2.COLOR_RGB2GRAY)
        # cv2.imwrite(outname, hm)
        plt.matshow(fig, cmap="gray", vmax=3, vmin=0)
        plt.colorbar(label="water depth")
        plt.clim(0, 3)
        plt.savefig(outname)
        plt.clf()

def calcu_mean(figs):
    plots = []
    for i, fig in enumerate(figs):
        # decrease dim
        if fig.shape[0] == 1:
            fig = fig[0]
        plots.append(np.mean(fig))

    return plots

def mean_plot(figures, outputpath):
    '''
    draw the plof fig for ground truth and predictions figs mean
    input: (ground truth, predictions), saving picture path
    output: None
    '''
    if not os.path.exists(outputpath):
        os.makedirs(outputpath)
    outname = os.path.join(outputpath, 'gt_preds_mean.jpg')
    print("[INFO] visuallization")
    gt, preds = figures
    gt_plots = calcu_mean(gt)
    preds_plots = calcu_mean(preds)
    x = np.arange(len(gt_plots))
    l1 = plt.plot(x, gt_plots, 'r--', label='type1')
    l2 = plt.plot(x, preds_plots, 'g--', label='type2')
    plt.plot(x, gt_plots, 'ro-', x, preds_plots, 'g+-')
    plt.legend()
    plt.savefig(outname)

# def wdept_check():
#     a, b, c = split_data()
#     nnn = []
#     for i in a[0]:
#         nnn.append(i[:,:,2])
#     save_viz(nnn, 'viztest')

# if __name__ == '__main__':
#     wdept_check()
#     figs = []
#     a = DataLoader()
#     for i in range(10):
#         mm = a.get_single()
#         ori, _ = list(mm)[0]
#         ori = ori[:,:,:,2].numpy()
#         figs.append(ori)
#     save_viz(figs, './viztest')