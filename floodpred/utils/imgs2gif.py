from itertools import filterfalse
import imageio
import os
import argparse

def filter_files(path):
    files = os.listdir(path)
    for i in files:
        if i =='gt_preds_mean.jpg':
            files.pop(files.index(i))
    
    return files

def files2gif(files, path):
    frames = []
    for file in files:
        frames.append(imageio.imread(os.path.join(path, file)))
    imageio.mimsave(os.path.join(path, 'out.gif'), frames, 'GIF', duration=0.2)

def main(args):
    path = args.path
    files = filter_files(path)
    files.sort()
    files2gif(files, path)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='train')
    parser.add_argument('--path', type=str, help='Path to images')
    args = parser.parse_args()

    main(args)

