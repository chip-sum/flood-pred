import numpy as np
import tensorflow as tf
from tensorflow import keras

def mean_wdepth_diff(y_true, y_pred):
    '''
    abs(mean(y_true(where true depth >1)) - mean(y_pred(where true depth >1)))
    '''
    x = np.ones((1264, 1008), dtype=np.float32)
    bool_mask = tf.math.less(x, y_true[0])
    float_mask = tf.cast(bool_mask, tf.float32)
    y_t = tf.math.multiply(y_true[0], float_mask)
    y_p = tf.math.multiply(y_pred[0], float_mask)

    nonzero_num = tf.math.count_nonzero(float_mask, dtype=tf.float32)
    mean_t = tf.math.reduce_sum(y_t)/nonzero_num
    mean_p =  tf.math.reduce_sum(y_p)/nonzero_num

    return tf.math.abs(mean_t - mean_p)
