import pickle
import numpy as np
import matplotlib.pyplot as plt

def main(path):
    with open(path, 'rb') as f:
        mm = pickle.load(f, encoding='latin1')
    # mm = pickle.load(path)

    print(mm.keys())
    exit()
    loss = mm['loss']
    mean_wdepth_diff = mm['mean_wdepth_diff']
    vall_loss = mm['val_loss']
    loss = np.log2(loss)
    mean_wdepth_diff = np.log2(mean_wdepth_diff)
    vall_loss = np.log2(vall_loss)
    x = np.arange(50)
    # l1 = plt.plot(x, loss, 'r--', label='train loss')
    # l2 = plt.plot(x, mean_wdepth_diff, 'g--', label='mean water depth diff')
    l3 = plt.plot(x, vall_loss, 'b--', label='validation loss')
    # plt.plot(x, loss, 'ro-', x, mean_wdepth_diff, 'g+-', x, vall_loss, 'bx-')
    plt.plot( x, vall_loss, 'bx-')
    
    plt.legend()
    plt.savefig("fig.jpg")


if __name__ == '__main__':
    main("history.pickle")
    