import h5py
import numpy as np


WINDOW = 2
TIME_DELTA = 5
PREDICT_SCENE="Scene_721"


class Scene():
    def __init__(self, name, data):
        self.name = name
        self.raining = data["raining"]
        self.waterdepth = data["waterdepth"]
        
    def __repr__(self):
        return "Scene: %s" % self.name


class H5FloodData():
    def __init__(self, path):
        h5 = h5py.File(path)
        self.topo = h5["Static/topo"]
        self.w, self.h = self.topo.shape
        
        self.landuse = h5["Static/landuse"] 
        self.manning = h5["Static/manning"]
        
        self.mask = (self.landuse[()] >= 2)
        self.scenes = [Scene(k, x) for k, x in h5.items() if k.startswith("Scene_")]    


def extract_feas(ds, cur_i, cur_j, scene_id, cur_T, pred_feas=None):
    # get landuse: num_fea=8+16
    mask_window = ds.mask[cur_i-WINDOW:cur_i+WINDOW+1, cur_j-WINDOW:cur_j+WINDOW+1]
    landuse_fea = (ds.landuse[cur_i-WINDOW:cur_i+WINDOW+1, cur_j-WINDOW:cur_j+WINDOW+1]*mask_window
                  ).flatten()

    # get topo diff: num_fea=8+16
    topo_fea = ((ds.topo[cur_i-WINDOW:cur_i+WINDOW+1, cur_j-WINDOW:cur_j+WINDOW+1] 
                - ds.topo[cur_i, cur_j])*mask_window).flatten()
    topo_fea = np.delete(topo_fea, (WINDOW**2+1)**2//2)

    # get landmanning
    manning_fea = (ds.manning[cur_i-WINDOW:cur_i+WINDOW+1, cur_j-WINDOW:cur_j+WINDOW+1]*mask_window
                  ).flatten()

    scene = ds.scenes[scene_id]
    # get rain
    rain_fea = scene.raining[cur_T-TIME_DELTA:cur_T+1][::-1].cumsum()
    # get simu 
    if pred_feas is not None:
        simu_fea = pred_feas[-TIME_DELTA: ] - pred_feas[-1-TIME_DELTA: -1]
    else:
        simu_fea = (scene.waterdepth[cur_T-TIME_DELTA:cur_T, cur_i, cur_j] 
            - scene.waterdepth[cur_T-TIME_DELTA-1:cur_T-1, cur_i, cur_j])

    features = np.concatenate([landuse_fea, topo_fea, manning_fea, rain_fea, simu_fea])
    
    y = scene.waterdepth[cur_T, cur_i, cur_j] - scene.waterdepth[cur_T-1, cur_i, cur_j]
    return features, y

def sample_one(ds, scene_ids, mask_indices=None):
    # choose i, j
    if mask_indices is None:
        cur_i = np.random.randint(WINDOW, ds.w-WINDOW)
        cur_j = np.random.randint(WINDOW, ds.h-WINDOW)
    else:
        m_i = np.random.randint(len(mask_indices))
        cur_i, cur_j = mask_indices[m_i]
    
    # sample scene
    scene_id = np.random.choice(scene_ids)
    time_length = ds.scenes[scene_id].raining.shape[0]
    cur_T = np.random.randint(TIME_DELTA+1, time_length)

    return extract_feas(ds, cur_i, cur_j, scene_id, cur_T)
        
def sample_trainset(ds, mask_indices, num=100):
    # train scenes
    train_scenes = [i for i, s in enumerate(ds.scenes) if s.name != PREDICT_SCENE]
    
    batch_feas, batch_label = tuple(zip(*[sample_one(ds, train_scenes, mask_indices) \
                                          for k in range(num)]))
    batch_feas, batch_label = np.asarray(batch_feas), np.asarray(batch_label)
    return batch_feas, batch_label
