import h5py
import numpy as np


def small_batch(h5set, static):
    rains = h5set['raining'][...]
    wdepth = h5set['waterdepth'][...]

    topo = static['topo'][...]
    maning = static['manning'][...]
    landuse = static['landuse'][...]
    print(type(topo), type(maning), type(wdepth[1]), type(rains))

    X,Y = [], []
    for i in range(5):
        m = np.zeros((1250, 1000, 5), dtype=np.float)
        m[:,:,0] = topo
        m[:,:,1] = maning
        m[:,:,2] = wdepth[i]
        m[:,:,3] = np.ones((1250,1000), dtype=np.float) * rains[i]
        land = np.where(landuse<2, 0, landuse)
        m[:,:,4] = np.where(land>1, 1, land)
        m = np.pad(m,((14,0), (8,0), (0,0)), 'constant')
        X.append(m)
        n = np.pad(wdepth[i+3],((14,0), (8,0)), 'constant')
        Y.append(n)

    return (X, Y) 

def data_parse(h5set, static, frames=5):
    rains = h5set['raining'][...]
    wdepth = h5set['waterdepth'][...]

    topo = static['topo'][...]
    maning = static['manning'][...]
    landuse = static['landuse'][...]

    X, Y = [], []
    for i in enumerate(wdepth):
        i = i[0]
        try: wdepth[i+frames]
        except: break
        m = np.zeros((1250, 1000, 5), dtype=np.float32)
        m[:,:,0] = topo
        m[:,:,1] = maning
        m[:,:,2] = wdepth[i]
        for j in range(frames):
            m[:,:,3] += rains[i+j]
        land = np.where(landuse<2, 0, landuse)
        m[:,:,4] = np.where(land>1, 1, land)
        m = np.pad(m,((14,0), (8,0), (0,0)), 'constant')
        X.append(m)
        n = np.pad(wdepth[i+frames],((14,0), (8,0)), 'constant')
        Y.append(n)

    return [X, Y]

def split_data(data_path=None):
    if data_path == None:
        raise KeyError("please specify h5 data path")
    f = h5py.File(data_path, 'r')
    g721 = f['Scene_721']
    gstatic = f['Static']
    gred = f['Scene_red']
    gorange = f['Scene_orange']
    gyellow = f['Scene_yellow']
    gblue = f['Scene_blue']

    tr_X, tr_Y = [], []

    tr = [gred,gorange,gyellow,gblue]
    for x in tr:
        temp1, temp2 = data_parse(x, gstatic)
        tr_X += temp1
        tr_Y += temp2
    
    valtest = data_parse(g721, gstatic)
    val_X = valtest[0][:20]
    val_Y = valtest[1][:20]
    test_X = valtest[0]
    test_Y = valtest[1]

    f.close()

    return (tr_X, tr_Y), (val_X, val_Y), (test_X, test_Y)

    smallset = small_batch(g721, gstatic)
    return smallset

def data_parseD(h5set, static, frames=5):
    rains = h5set['raining'][...]
    wdepth = h5set['waterdepth'][...]

    topo = static['topo'][...]
    maning = static['manning'][...]
    landuse = static['landuse'][...]

    X, Y = [], []
    for i in enumerate(wdepth):
        i = i[0]
        try: wdepth[i+frames]
        except: break
        m = np.zeros((1250, 1000, 4), dtype=np.float32)
        r = np.zeros(6, dtype=np.float32)
        m[:,:,0] = topo
        m[:,:,1] = maning
        m[:,:,2] = wdepth[i]
        for j in range(frames):
            # m[:,:,3] += rains[i+j]
            r[j] = np.mean(rains[i+j])
        # r[-1] = -1.7 ## ?
        land = np.where(landuse<2, 0, landuse)
        m[:,:,3] = np.where(land>1, 1, land)
        m = np.pad(m,((14,0), (8,0), (0,0)), 'constant')
        X.append((m, r,))
        n = np.pad(wdepth[i+frames],((14,0), (8,0)), 'constant')
        Y.append(n)

    return [X, Y]

def split_data_D(data_path = None):
    if data_path == None:
        raise KeyError("please specify h5 data path")
    f = h5py.File(data_path, 'r')
    g721 = f['Scene_721']
    gstatic = f['Static']
    gred = f['Scene_red']
    gorange = f['Scene_orange']
    gyellow = f['Scene_yellow']
    gblue = f['Scene_blue']

    tr_X, tr_Y = [], []

    tr = [gred,gorange,gyellow,gblue]
    for x in tr:
        temp1, temp2 = data_parseD(x, gstatic)
        tr_X += temp1
        tr_Y += temp2
    
    valtest = data_parseD(g721, gstatic)
    val_X = valtest[0][:20]
    val_Y = valtest[1][:20]
    test_X = valtest[0]
    test_Y = valtest[1]

    f.close()

    return (tr_X, tr_Y), (val_X, val_Y), (test_X, test_Y)

# if __name__ == '__main__':

#     split_data()