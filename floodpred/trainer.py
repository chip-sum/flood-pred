# -*- coding=utf-8 -*-

from tensorflow import keras
import time


class LossHistory(keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.losses = []
    
    def on_batch_end(self, batch, logs={}):
        pass

    def on_epoch_end(self, epoch, logs=None):
        print_loss = logs.get('loss')
        # val_loss = logs.get('val_loss')
        print_loss = str(print_loss)
        with open("./trainlog/log", 'a+') as f:
            f.write(print_loss+'\n')


class Trainer():
    def __init__(self):
        self.history = LossHistory()

    def train(self, agent, trainset, outpath, val=None):
        agent.learn_from(trainset, self.history, outpath, val)

    def test(self, agent, testset):
        agent.test(testset)

    def pred(self, agent, test_frames):
        '''
        return: groundtruth water depth numpy array and prediction numpy array
        '''
        start = time.time()
        ans = agent.predict(test_frames)
        end = time.time()
        print("prediction time cost {} seconds".format(end-start))
        return ans