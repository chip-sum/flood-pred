# -*- coding=utf-8 -*-

import argparse

from agent import Agent
from dataloader import DataLoader
from dataloader_D import DataLoader_D
from trainer import Trainer


def main(args):
    model = args.model_type
    out_path = args.output_path
    if model == 'UnetD':
        loader = DataLoader_D('../data/thu-flood-simulation.h5')
        # (1250, 1000) to (1264, 1008)
        agent = Agent(name='UnetD', input_shape=[(1264,1008,4), (6,)])
    else:
        loader = DataLoader('../data/thu-flood-simulation.h5')
        agent = Agent(name='Unet', input_shape=(1264,1008,5))
    trainer = Trainer()
    trainer.train(agent, loader.trainset, out_path, val=loader.testset, )
    # trainer.test(agent, loader.testset)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='train')
    parser.add_argument('--model-type', type=str, default='UnetD', help='Unet or UnetD')
    parser.add_argument('--output-path', type=str, help='./model_UnetD/5dim_wdepth_300')
    # parser.add_argument()
    args = parser.parse_args()

    main(args)
