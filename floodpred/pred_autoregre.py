# -*- coding=utf-8 -*-

from agent import Agent
from dataloader import DataLoader
from dataloader_D import DataLoader_D
from trainer import Trainer
from utils.viz_utils import save_viz, mean_plot
import argparse


def predD(args):
    model_path = args.model_path
    out_path = args.output_path
    vis_all = args.vis_all

    agent = Agent(name='UnetD', input_shape=[(1264, 1008, 4), (6,)])

    trainer = Trainer()
    agent.load(model_path)

    loader = DataLoader_D(
        '/home/jayfu/2022/flood-pred/data/thu-flood-simulation.h5'
        )
    
    test_frames = loader.get_pred(num=139)
        # test_frames = list(test_frames)[80:130]
        # pp = test_frames[-1]
        # input1 = pp[0]['input_1'].numpy()
        # input2 = pp[0]['input_2'].numpy()
        # y = pp[1].numpy()
        # x = {'input_1':input1, 'input_2':input2}
        # pp = trainer.pred(agent, x)
        # save_viz([pp,y], "./vistest")
        # exit()
    # choose specified frames
    test_frames = list(test_frames)[30:80]
    
    input1 = test_frames[0][0]['input_1'].numpy()
    input2 = test_frames[0][0]['input_2'].numpy()
    y = test_frames[0][1].numpy()
    x = {'input_1':input1, 'input_2':input2}
    pred = trainer.pred(agent, x)
    preds = [pred]
    gt = [y]
    a = 21

    for i in range(5,len(test_frames),5):  
        pred = pred.reshape(1, 1264, 1008)
        input1 = test_frames[i][0]['input_1'].numpy()
        input2 = test_frames[i][0]['input_2'].numpy()
        y = test_frames[i][1].numpy()
        gt.append(y)
        input1[:,:,:,2] = pred
        x = {'input_1':input1, 'input_2':input2}
        pred = trainer.pred(agent, x)
        preds.append(pred)
    # mean_plot((gt, preds), outputpath=out_path)
    # if vis_all:
    #     save_viz(preds, outputpath=out_path)
        # save_viz(gt, 'unetD_80-130gt_5frames')

def main(args): 
    model_path = args.model_path
    out_path = args.output_path
    vis_all = args.vis_all
    agent = Agent(name='Unet', input_shape=(1264, 1008, 4))

    trainer = Trainer()
    agent.load(model_path)

    loader = DataLoader(
        '/home/jayfu/2022/flood-pred/data/thu-flood-simulation.h5'
        )
    
    test_frames = loader.get_pred(num=139)
    # choose specified frames
    test_frames = list(test_frames)[30:80]
    
    input1 = test_frames[0][0]['input_1'].numpy()
    input2 = test_frames[0][0]['input_2'].numpy()
    y = test_frames[0][1].numpy()
    x = {'input_1':input1, 'input_2':input2}
    pred = trainer.pred(agent, x)
    preds = [pred]
    gt = [y]

    for i in range(5,50,5):  
        pred = pred.reshape(1,1264, 1008)
        x = test_frames[i][0].numpy()
        y = test_frames[i][1].numpy()
        gt.append(y)
        x[:,:,:,2] = pred
        pred = trainer.pred(agent, x)
        preds.append(pred)
    
    # mean_plot((gt, preds), outputpath=out_path)
    # if vis_all:
        # save_viz(preds, outputpath=out_path)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='prediction')
    parser.add_argument('--model-path', type=str, default='./model_UnetD/5dim_wdepth_300')
    parser.add_argument('--output-path', type=str, default='./vis_output/5dim_wdepth_300')
    parser.add_argument('--vis-all', type=bool, default=False)
    args = parser.parse_args()
    if 'UnetD' in args.model_path: predD(args)
    else: main(args)
    