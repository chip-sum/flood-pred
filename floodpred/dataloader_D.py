# -*- coding=utf-8 -*-
import tensorflow as tf
from tensorflow.data import Dataset

from utils.data_utils import split_data_D


class DataLoader_D():
    def __init__(self, path=None):
        print('[INFO]start initializing dataset for UNET with Dense')
        trainset, valset, testset = split_data_D(path)
        def _gen_tr():
            for i in range(len(trainset[0])):
                x = trainset[0][i][0]
                r = trainset[0][i][1]
                y = trainset[1][i]
                yield {"input_1":x, "input_2":r}, y
        def _gen_te():
            for i in range(len(testset[0])):
                x = trainset[0][i][0]
                r = trainset[0][i][1]
                y = testset[1][i]
                yield {"input_1":x, "input_2":r}, y
        def _gen_va():
            for i in range(len(valset[0])):
                x = trainset[0][i][0]
                r = trainset[0][i][1]
                y = valset[1][i]
                yield {"input_1":x, "input_2":r}, y

        # self.trainset = Dataset.from_tensor_slices(trainset)

        self.trainset = Dataset.from_generator(
                lambda: _gen_tr(),
                output_signature=(
                    {"input_1": tf.TensorSpec(shape=(1264, 1008, 4), dtype=tf.float32),
                     "input_2":tf.TensorSpec(shape=(6,), dtype=tf.float32),},
                    tf.TensorSpec(shape=(1264, 1008), dtype=tf.float32))
                )
                                            
        self.trainset = self.trainset.shuffle(buffer_size=16)
        self.trainset = self.trainset.batch(1)
        print('[INFO] trainset ready')

        # self.valset = Dataset.from_generator(lambda:valset,
        #                                      output_signature=tf.TensorSpec(shape=(), dtype=tf.int32))
        self.valset = Dataset.from_generator(
                lambda: _gen_va(),
                output_signature=(
                    {"input_1": tf.TensorSpec(shape=(1264, 1008, 4), dtype=tf.float32),
                     "input_2":tf.TensorSpec(shape=(6,), dtype=tf.float32),},
                    tf.TensorSpec(shape=(1264, 1008), dtype=tf.float32))
                )
        self.valset = self.valset.batch(1)
        print('[INFO] valset ready')
        self.testset = Dataset.from_generator(
                lambda: _gen_te(),
                output_signature=(
                    {"input_1": tf.TensorSpec(shape=(1264, 1008, 4), dtype=tf.float32),
                     "input_2":tf.TensorSpec(shape=(6,), dtype=tf.float32),},
                    tf.TensorSpec(shape=(1264, 1008), dtype=tf.float32))
                )
        self.testset = self.testset.batch(1)
        print('[INFO] testset ready')
        # self.testset = Dataset.from_tensor_slices(testset)
        print('[INFO] dataset all ready, there are {} data in trainset,\
               20 data in valset, {} data in testset'.format(
                   len(trainset[0]), len(testset[0])
               ))


    def get_batch(self):
        pass

    def get_pred(self, num):
        return self.testset.take(num)


if __name__ == '__main__':
    a = DataLoader()
    
    