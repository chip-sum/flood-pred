from unittest import main
import h5py
import matplotlib.pyplot as plt
import numpy as np
from scipy import sparse

import xgboost as xgb
from utils.xgbt_utils import *

def main():
    np.random.seed(1)
    H5_PATH = "../data/thu-flood-simulation.h5"
    ds = H5FloodData(H5_PATH)

    # Feature hyper parameter
    WINDOW = 2
    TIME_DELTA = 5
    PREDICT_SCENE="Scene_721"
    feature_types = ["c"]*((WINDOW**2+1)**2) \
        + ["float"]*((WINDOW**2+1)**2-1+(WINDOW**2+1)**2+TIME_DELTA*2+1)
    feature_names = ["landuse_%s" % i for i in range((WINDOW**2+1)**2)]  \
        + ["topo_%s" % i for i in range((WINDOW**2+1)**2-1)] \
        + ["manning_%s" % i for i in range((WINDOW**2+1)**2)] \
        + ["raining_%s" % i for i in range(TIME_DELTA + 1)] \
        + ["simu_%s" % i for i in range(TIME_DELTA)]

    s = sparse.coo_matrix(ds.mask)
    mask_indices = list(zip(s.row, s.col))
    mask_indices_ij = list(map(np.asarray, list(zip(*mask_indices))))
        
    train_fea, train_label = sample_trainset(ds, mask_indices, num=200000)

    param = {'max_depth':6, 'eta':0.3, 'objective':'reg:squarederror' }
    num_round = 500

    dtrain = xgb.DMatrix(train_fea, 
                         label=train_label, 
                         feature_names=feature_names,
                         enable_categorical=True, 
                         feature_types=feature_types)
    bst = xgb.train(param, dtrain, num_round)

    # Make Predictions
    label_maps = []
    pred_maps = []

    for i, s in enumerate(ds.scenes):
        if s.name == PREDICT_SCENE:
            predict_scene_id = i
            time_length = ds.scenes[predict_scene_id].raining.shape[0]

    # s_i, e_i, s_j, e_j = 600, 1200, 200, 600
    s_i, e_i, s_j, e_j = WINDOW, ds.w - WINDOW, WINDOW, ds.h - WINDOW
            
    for t in range(35, 70):
    # for t in range(TIME_DELTA+1, TIME_DELTA+5):
        print("Predicting timestamp: %s ..." % t)
        
        pred_feas = np.asarray(pred_maps)
        
        def get_fea_label_one(ij):
            i, j = ij
            pred_feas_i = None if len(pred_maps) <= 5 else pred_feas[:, i, j]
            fea, label = extract_feas(ds, i, j, 
                                      predict_scene_id, 
                                      t, 
                                      pred_feas=pred_feas_i)
            return fea, label

        test_feas, test_label = list(zip(*map(get_fea_label_one, mask_indices)))
        test_label = np.asarray(test_label)
        test_fea = np.asarray(test_feas)
        
        dtest = xgb.DMatrix(test_fea, 
                            label=test_label, 
                            feature_names=feature_names,
                            enable_categorical=True, 
                            feature_types=feature_types)
        
        pred_map = sparse.coo_matrix((bst.predict(dtest), 
                                     (mask_indices_ij[0], 
                                     mask_indices_ij[1])), 
                                     shape=[ds.w, ds.h]).toarray()
        label_map = sparse.coo_matrix((test_label, 
                                       (mask_indices_ij[0], 
                                        mask_indices_ij[1])), 
                                       shape=[ds.w, ds.h]).toarray()
        pred_maps.append(pred_map)
        label_maps.append(label_map)

    waterdepth_label = np.cumsum(
        [ds.scenes[0].waterdepth[34]*ds.mask] + label_maps, axis=0)
    waterdepth_pred = np.cumsum(
        [ds.scenes[0].waterdepth[34]*ds.mask] + pred_maps, axis=0)

if __name__ == '__main__':
    main()
    