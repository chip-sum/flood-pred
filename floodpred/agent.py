# -*- coding=utf-8 -*-

from image2image.unet import UnetAgent
from image2image.unetD import UnetDAgent


class Agent():
    def __init__(
        self, 
        name=None, 
        input_shape=(1250,1000,4)):
        self.agent = None
        name = name + 'Agent'
        agentklass = globals()[name]
        self.model = agentklass(input_shape)

    def __repr__(self):
        return "[INFO]This is an agent powered by {self.name}"

    def learn_from(self, data, LossHistory, outpath, val=None):
        if self.model == None:
            raise KeyError("You should specify an agent")

        self.model.learn_from(data, LossHistory, val, outpath)

    def test(self, data):
        self.model.test(data)

    def testpr(self):
        self.model.testpr()
    
    def load(self, path):
        self.model.load(path)

    def predict(self, data):
        return self.model.predict(data)

if __name__ == '__main__':
    a = Agent(name='Unet')
    a.testpr()
    